/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import modelos.Directiva;
import modelos.DirectivaDAO;

/**
 *
 * @author david
 */
public class DirectivaJDBC implements DirectivaDAO {

    Connection con;

    private static final String SELECT_ALL_DIRECTIVA_PAGINATION = "select * from directiva order by idDirectiva limit ? offset ?";
    private static final String SELECT_ALL_DIRECTIVA = "select * from directiva";
    private static final String SELECT_DIRECTIVA = "select * from directiva where idDirectiva = ?";
    private static final String SELECT_DIRECTIVA_BY_NOMBRE = "select * from directiva where nombre LIKE ? order by idDirectiva limit ? offset ?";
    private static final String SELECT_DIRECTIVA_BY_EDAD = "select * from directiva where edad = ? order by idDirectiva limit ? offset ?";
    private static final String INSERT_DIRECTIVA_QUERY = "insert into directiva (nombre, edad, salario , puesto)values";
    private static final String DELETE_DIRECTIVA = "delete from directiva where idDirectiva = ?";
    private static final String UPDATE_DIRECTIVA = "update directiva SET nombre = ?, edad= ?, salario = ? , puesto = ?  where idDirectiva = ?";
    private static final String SELECT_LAST_ID = "select MAX(idDirectiva) as idDirectiva from directiva";

    public DirectivaJDBC(Connection conn) {
        super();
        this.con = conn;
    }

    @Override
    public List<Directiva> listaDirectivos(int desde, int cuantos) {

        PreparedStatement stmt;
        List<Directiva> directiva = new ArrayList<Directiva>();

        try {

            stmt = con.prepareStatement(SELECT_ALL_DIRECTIVA_PAGINATION);
            stmt.setInt(1, cuantos);
            stmt.setInt(2, desde);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {

                int idDirectivo = rs.getInt("idDirectiva");
                String nombreDirectivo = rs.getString("nombre");
                int edadDirectivo = rs.getInt("edad");
                double salarioDirectivo = rs.getDouble("salario");
                String puestoDirectivo = rs.getString("puesto");

                Directiva directivos = new Directiva(nombreDirectivo, edadDirectivo, salarioDirectivo, puestoDirectivo);
                directivos.setIdDirectiva(idDirectivo);
                directiva.add(directivos);

            }

        } catch (SQLException excpt) {

            excpt.printStackTrace();
            System.err.print(excpt);
            directiva = null;

        }
        System.out.println(directiva);
        return directiva;
    }

    @Override
    public boolean insertDirectivos(String nombre, int edad, double salario, String puesto) {

        Statement stmt = null;
        Statement stmt2 = null;
        Directiva directivo = null;
        
        int numero;
        boolean comprobar = false;

        try {

            stmt2 = con.createStatement();
            ResultSet rs2 = stmt2.executeQuery(SELECT_LAST_ID);
            rs2.last();

            int idDirectiva = rs2.getInt("idDirectiva");
            idDirectiva += 1;
            stmt = con.createStatement();
            
           // numero = stmt.executeUpdate(INSERT_FISIOTERAPEUTAS_QUERY + "(\"" + nombre + " \", " + edad + " ,\" " + especialidad + " \", " + salario + ")");
                  numero = stmt.executeUpdate(INSERT_DIRECTIVA_QUERY + " (\"" + nombre + " \", " + edad + " , " + salario + ", \" " + puesto + " \")");

            if (numero == 1) {
                comprobar = true;
                directivo = new Directiva(nombre, edad, salario, puesto);
                System.out.println(directivo);

            } else {

                System.err.print("ERROR, no hemos podido insertar la fila");
                directivo = null;
                comprobar = false;
            }

        } catch (SQLException excpt) {

            excpt.printStackTrace();

        } finally {

            try {

                if (stmt != null) {
                    stmt.close();
                }
                if (stmt2 != null) {
                    stmt2.close();
                }

            } catch (SQLException ex) {

                ex.printStackTrace();
            }
        }

        return comprobar;
    }

    @Override
    public boolean updateDirectivos(Directiva directivos) {

        boolean salida = false;
        Directiva directivoUpdate = getDirectivos(directivos.getIdDirectiva());

        if (directivoUpdate != null) {

            if (directivos.getNombre().equals(directivoUpdate.getNombre()) && directivos.getEdad() == directivoUpdate.getEdad() && directivos.getSalario() == directivoUpdate.getSalario() && directivos.getPuesto().equals(directivoUpdate.getPuesto())) {
                System.out.println("Los valores introducidos son exactamente los mismos a los anteriores");
            } else {

                PreparedStatement preparedStatement;

                try {

                    preparedStatement = con.prepareStatement(UPDATE_DIRECTIVA);
                    preparedStatement.setString(1, directivos.getNombre());
                    preparedStatement.setInt(2, directivos.getEdad());
                    preparedStatement.setDouble(3, directivos.getSalario());
                    preparedStatement.setString(4, directivos.getPuesto());
                    preparedStatement.setInt(5, directivos.getIdDirectiva());

                    int numero = preparedStatement.executeUpdate();

                    if (numero == 1) {

                        directivoUpdate = directivos;
                        salida = true;
                        System.out.println("Hemos actualizado al directivo ");

                    } else {

                        directivoUpdate = null;
                        salida = false;
                        System.out.println("No hemos podido actualizar al directivo");
                    }

                } catch (SQLException excpt) {

                    excpt.printStackTrace();
                }
            }
        } else {

            System.out.println("Error, no existe un directivo con ese ID");
        }

        System.out.println(salida);
        return salida;
    }

    @Override
    public boolean deleteDirectivos(int idDirectiva) {

        Directiva directiva = getDirectivos(idDirectiva);
        boolean salida = false;

        if (directiva != null) {

            PreparedStatement stmt = null;

            try {

                stmt = con.prepareStatement(DELETE_DIRECTIVA);
                stmt.setInt(1, idDirectiva);
                int numero = stmt.executeUpdate();

                if (numero == 1) {
                    salida = true;
                    System.out.println("Hemos borrado al directivo con ID " + idDirectiva + " al carreeeeeeeeeeeer");

                } else {

                    salida = false;
                    System.out.println("Eror");

                }
            } catch (SQLException excpt) {

                excpt.printStackTrace();

            } finally {

                try {

                    if (stmt != null) {
                        stmt.close();
                    }

                } catch (SQLException ex) {

                    ex.printStackTrace();
                }
            }
        } else {
            salida = false;
            System.out.println("ERROR, no hemos podido encontrar un directivo con ese ID");

        }
        return salida;
    }

    @Override
    public Directiva getDirectivos(int idDirectiva) {

        PreparedStatement stmt;
        Directiva directivo = null;

        try {

            stmt = con.prepareStatement(SELECT_DIRECTIVA);
            stmt.setInt(1, idDirectiva);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {

                int idDirectivo = rs.getInt("idDirectiva");
                String nombreDirectivo = rs.getString("nombre");
                int edadDirectivo = rs.getInt("edad");
                double salarioDirectivo = rs.getDouble("salario");
                String puestoDirectivo = rs.getString("puesto");

                directivo = new Directiva(nombreDirectivo, edadDirectivo, salarioDirectivo, puestoDirectivo);
                directivo.setIdDirectiva(idDirectiva);

            }

        } catch (SQLException excpt) {

            excpt.printStackTrace();
            System.err.print(excpt);
            directivo = null;
        }

        System.out.println(directivo);
        return directivo;
    }

    @Override
    public List<Directiva> getDirectivoLike(String nombre, int desde, int cuantos) {

        List<Directiva> salida = new ArrayList<Directiva>();
        PreparedStatement prepraredStatement;

        try {

            prepraredStatement = con.prepareStatement(SELECT_DIRECTIVA_BY_NOMBRE);
            prepraredStatement.setString(1, "%" + nombre + "%");
            prepraredStatement.setInt(2, desde);
            prepraredStatement.setInt(3, cuantos);

            ResultSet rs = prepraredStatement.executeQuery();

            while (rs.next()) {

                int idDirectivo = rs.getInt("idDirectiva");
                String nombreDirectivo = rs.getString("nombre");
                int edadDirectivo = rs.getInt("edad");
                double salarioDirectivo = rs.getDouble("salario");
                String puestoDirectivo = rs.getString("puesto");

                Directiva directivo = new Directiva(nombreDirectivo, edadDirectivo, salarioDirectivo, puestoDirectivo);
                directivo.setIdDirectiva(idDirectivo);
                salida.add(directivo);

            }

            if (salida.isEmpty()) {

                salida = null;

            }

        } catch (SQLException excpt) {

            excpt.printStackTrace();
        }

        System.out.println(salida);
        return salida;
    }

    @Override
    public List<Directiva> getDirectivaByEdad(int edad, int desde, int cuantos) {

        List<Directiva> salida = new ArrayList<Directiva>();
        PreparedStatement preparedstatement;

        try {

            preparedstatement = con.prepareStatement(SELECT_DIRECTIVA_BY_EDAD);
            preparedstatement.setInt(1, edad);
            preparedstatement.setInt(2, desde);
            preparedstatement.setInt(3, cuantos);

            ResultSet rs = preparedstatement.executeQuery();

            while (rs.next()) {
                int idDirectivo = rs.getInt("idDirectiva");
                String nombreDirectivo = rs.getString("nombre");
                int edadDirectivo = rs.getInt("edad");
                double salarioDirectivo = rs.getDouble("salario");
                String puestoDirectivo = rs.getString("puesto");
                
                Directiva directivos = new Directiva(nombreDirectivo, edadDirectivo, salarioDirectivo, puestoDirectivo);
                directivos.setIdDirectiva(idDirectivo);
                salida.add(directivos);
                
            }
            
            if(salida.isEmpty()){
                
                salida = null;
            }
            
        } catch (SQLException excpt){

            excpt.printStackTrace();
        }
        
        System.out.println(salida);
        return salida;
    }

    @Override
    public int allDirectivos() {

        Statement stmt;
        int conteo = 0;

        try {

            stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(SELECT_ALL_DIRECTIVA);

            while (rs.next()) {
                conteo++;
            }

        } catch (SQLException excpt) {

            excpt.printStackTrace();
        }

        System.out.println(conteo);
        return conteo;
    }

    @Override
    public int lastID() {

        Statement stmt;

        try {
            stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(SELECT_LAST_ID);

            while (rs.next()) {
                System.out.println("El ultimo ID es: " + rs.getString(1));
            }

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        }
        return 0;
    }

}
