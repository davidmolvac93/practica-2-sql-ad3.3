/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import modelos.FisioterapeutasDAO;
import modelos.Fisioterapeutas;

/**
 *
 * @author david
 */
public class FisioterapeutaJDBC implements FisioterapeutasDAO {

    Connection conn;

    private static final String SELECT_ALL_FISIOTERAPEUTAS_PAGINATION = "select * from fisioterapeutas order by idFisioterapeuta limit ? offset ?";
    private static final String SELECT_ALL_FISIOTERAPEUTAS = "select * from fisioterapeutas";
    private static final String SELECT_FISIOTERAPEUTAS = "select * from fisioterapeutas where idFisioterapeuta = ?";
    private static final String SELECT_FISIOTERAPEUTAS_BY_NOMBRE = "select * from fisioterapeutas where nombre LIKE ? order by idFisioterapeuta limit ? offset ?";
    private static final String SELECT_FISIOTERAPEUTAS_BY_EDAD = "select * from fisioterapeutas where edad = ? order by idFisioterapeuta limit ? offset ?";
    private static final String INSERT_FISIOTERAPEUTAS_QUERY = "insert into fisioterapeutas (nombre, edad, especialidad , salario) values";
    private static final String DELETE_FISIOTERAPEUTAS = "delete from fisioterapeutas where idFisioterapeuta = ?";
    private static final String UPDATE_FISIOTERAPEUTAS = "update fisioterapeutas SET nombre = ?, edad= ?, especialidad = ? , salario = ? where idFisioterapeuta = ?";
    private static final String SELECT_LAST_ID = "select MAX(idFisioterapeuta) as idFisioterapeuta from fisioterapeutas";

    public FisioterapeutaJDBC(Connection conn) {

        super();
        this.conn = conn;

    }

    @Override
    public List<Fisioterapeutas> listaFisios(int desde, int cuantos) {

        PreparedStatement stmt;
        List<Fisioterapeutas> fisios = new ArrayList<Fisioterapeutas>();

        try {

            stmt = conn.prepareStatement(SELECT_ALL_FISIOTERAPEUTAS_PAGINATION);
            stmt.setInt(1, desde);
            stmt.setInt(2, cuantos);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {

                int idFisio = rs.getInt("idFisioterapeuta");
                String nombreFisio = rs.getString("nombre");
                int edadFisio = rs.getInt("edad");
                String especialidadFisio = rs.getString("especialidad");
                double salarioFisio = rs.getDouble("salario");

                Fisioterapeutas fisio = new Fisioterapeutas(nombreFisio, edadFisio, especialidadFisio, salarioFisio);
                fisio.setIdFisioterapueta(idFisio);
                fisios.add(fisio);

            }

        } catch (SQLException excpt) {

            excpt.printStackTrace();
            System.err.print(excpt);
            fisios = null;

        }

        System.out.println(fisios);
        return fisios;
    }

    @Override
    public boolean insertFisio(String nombre, int edad, String especialidad, double salario) {

        Statement stmt = null;
        Statement stmt2 = null;
        Fisioterapeutas fisio = null;

        int numero;
        boolean comprobar = false;

        try {

            stmt2 = conn.createStatement();
            ResultSet rs2 = stmt2.executeQuery(SELECT_LAST_ID);
            rs2.last();

            int idFisio = rs2.getInt("idFisioterapeuta");
            idFisio += 1;
            stmt = conn.createStatement();

            numero = stmt.executeUpdate(INSERT_FISIOTERAPEUTAS_QUERY + "(\"" + nombre + " \", " + edad + " ,\" " + especialidad + " \", " + salario + ")");

            if (numero == 1) {

                comprobar = true;
                fisio = new Fisioterapeutas(nombre, edad, especialidad, salario);
                System.out.println("Hemos insertado el nuevo fisioterapeuta");

            } else {

                comprobar = false;
                fisio = null;
                System.out.println("Error no hemos podido introducir la fila");

            }

        } catch (SQLException excpt) {

            excpt.printStackTrace();
            System.err.print(excpt);

        } finally {

            try {

                if (stmt != null) {
                    stmt.close();
                }
                if (stmt2 != null) {
                    stmt2.close();
                }

            } catch (SQLException ex) {

                ex.printStackTrace();
                System.err.print(ex);

            }
        }

        System.out.println(comprobar);
        return comprobar;
    }

    @Override
    public boolean updateFisio(Fisioterapeutas fisio) {

        boolean salida = false;
        Fisioterapeutas fisioUpdate = getFisio(fisio.getIdFisioterapueta());

        if (fisioUpdate != null) {

            if (fisio.getNombre().equals(fisioUpdate.getNombre()) && fisio.getEdad() == fisioUpdate.getEdad() && fisio.getEspecialidad().equals(fisioUpdate.getEspecialidad()) && fisio.getSalario() == fisioUpdate.getSalario()) {

                System.out.println("Los valores introducidos son los mismo a los anteriores");

            } else {

                PreparedStatement preparedStatement;

                try {

                    preparedStatement = conn.prepareStatement(UPDATE_FISIOTERAPEUTAS);
                    preparedStatement.setString(1, fisioUpdate.getNombre());
                    preparedStatement.setInt(2, fisioUpdate.getEdad());
                    preparedStatement.setString(3, fisioUpdate.getEspecialidad());
                    preparedStatement.setDouble(4, fisioUpdate.getSalario());
                    preparedStatement.setInt(5, fisioUpdate.getIdFisioterapueta());

                    int numero = preparedStatement.executeUpdate();

                    if (numero == 1) {

                        fisio = fisioUpdate;
                        salida = true;
                        System.out.println("Hemos actualizado al fisioterapeuta");

                    } else {

                        fisioUpdate = null;
                        salida = true;
                        System.out.println("No hemos podido actualizar los valores");

                    }

                } catch (SQLException excpt) {

                    excpt.printStackTrace();
                    System.err.print(excpt);
                }
            }

        } else {

            System.out.println("Error, no existe un fisioterapeuta con ese ID");
        }

        System.out.println(salida);
        return salida;
    }

    @Override
    public boolean deleteFisio(int idFisioterapeuta) {

        Fisioterapeutas fisioterapeuta = getFisio(idFisioterapeuta);
        boolean salida = false;

        if (fisioterapeuta != null) {

            PreparedStatement stmt = null;

            try {

                stmt = conn.prepareStatement(DELETE_FISIOTERAPEUTAS);
                stmt.setInt(1, idFisioterapeuta);
                int numero = stmt.executeUpdate();

                if (numero == 1) {

                    salida = true;
                    System.out.println("Hemos borrado al entrenador con ID " + idFisioterapeuta + " al carreeeeeeeeer");

                } else {

                    salida = false;

                }

            } catch (SQLException excpt) {

                excpt.printStackTrace();
                System.err.print(excpt);

            } finally {

                try {

                    if (stmt != null) {

                        stmt.close();
                    }

                } catch (SQLException ex) {

                    ex.printStackTrace();
                    System.err.print(ex);

                }
            }

        } else {

            System.err.print("ERROR, no hemos podido encontrar un fisioterapeuta con el ID " + idFisioterapeuta);
            salida = false;

        }

        return salida;
    }

    @Override
    public Fisioterapeutas getFisio(int idFisioterapeuta) {

        PreparedStatement stmt;
        Fisioterapeutas fisios = null;

        try {

            stmt = conn.prepareStatement(SELECT_FISIOTERAPEUTAS);
            stmt.setInt(1, idFisioterapeuta);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {

                int idFisio = rs.getInt("idFisioterapeuta");
                String nombreFisio = rs.getString("nombre");
                int edadFisio = rs.getInt("edad");
                String especialidadFisio = rs.getString("especialidad");
                double salarioFisio = rs.getDouble("salario");

                fisios = new Fisioterapeutas(nombreFisio, edadFisio, especialidadFisio, salarioFisio);
                fisios.setIdFisioterapueta(idFisioterapeuta);
            }

        } catch (SQLException excpt) {

            excpt.printStackTrace();
            System.err.print(excpt);

        }

        System.out.println(fisios);
        return fisios;
    }

    @Override
    public List<Fisioterapeutas> getFisoterapeutasLike(String nombre, int desde, int cuantos) {

        List<Fisioterapeutas> salida = new ArrayList<Fisioterapeutas>();
        PreparedStatement preparedStatement;

        try {

            preparedStatement = conn.prepareStatement(SELECT_FISIOTERAPEUTAS_BY_NOMBRE);
            preparedStatement.setString(1, "%" + nombre + "%");
            preparedStatement.setInt(2, desde);
            preparedStatement.setInt(3, cuantos);

            ResultSet rs = preparedStatement.executeQuery();
            
            while(rs.next()){
                
                int idFisio = rs.getInt("idFisioterapeuta");
                String nombreFisio = rs.getString("nombre");
                int edadFisio = rs.getInt("edad");
                String especialidadFisio = rs.getString("especialidad");
                double salarioFisio = rs.getDouble("salario");
                
                Fisioterapeutas fisio = new Fisioterapeutas(nombreFisio, edadFisio, especialidadFisio, salarioFisio);
                fisio.setIdFisioterapueta(idFisio);
                salida.add(fisio);
                
            }

            if(salida.isEmpty()){
                
                salida = null;
            }
            
        } catch  (SQLException excpt){

            excpt.printStackTrace();
            System.err.print(excpt);
        }

        System.out.println(salida);
        return salida;
    }

    @Override
    public List<Fisioterapeutas> getFisoterapeutasByEdad(int edad, int desde, int cuantos) {
        
        List<Fisioterapeutas> fisio = new ArrayList<Fisioterapeutas>();
        PreparedStatement preparedStatement;
        
        try{
            
            preparedStatement = conn.prepareStatement(SELECT_FISIOTERAPEUTAS_BY_EDAD);
            preparedStatement.setInt(1, edad);
            preparedStatement.setInt(2, desde);
            preparedStatement.setInt(3, cuantos);
            
            ResultSet rs = preparedStatement.executeQuery();

            while(rs.next()){
                
                int idFisio = rs.getInt("idFisioterapeuta");
                String nombreFisio = rs.getString("nombre");
                int edadFisio = rs.getInt("edad");
                String especialidadFisio = rs.getString("especialidad");
                double salarioFisio = rs.getDouble("salario");
                
                Fisioterapeutas fisios = new Fisioterapeutas(nombreFisio, edadFisio, especialidadFisio, salarioFisio);
                fisios.setIdFisioterapueta(idFisio);
                fisio.add(fisios);
                System.out.println(fisios);
                
                if(fisio.isEmpty()){
                    
                    fisio = null;
                    
                }
                
            }
        }catch (SQLException excpt){
            
            excpt.printStackTrace();
            System.err.print(excpt);
            
        }

        System.out.println(fisio);
        return fisio;
    }

    @Override
    public int allFisoterapeutas() {
        
        Statement stmt;
        int conteo = 0;

        try {

            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(SELECT_ALL_FISIOTERAPEUTAS);

            while (rs.next()) {
                conteo++;
            }

        } catch (SQLException excpt) {

            excpt.printStackTrace();
            System.err.print(excpt);
        }

        System.out.println(conteo);
        return conteo;
    }

    @Override
    public int lastID() {

        Statement stmt;

        try {
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(SELECT_LAST_ID);

            while (rs.next()) {
                System.out.println("El ultimo ID es: " + rs.getString(1));
            }

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        }
        return 0;

    }

}
