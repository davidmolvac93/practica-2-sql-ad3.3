/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tests;

import modelos.Entrenadores;
import modelos.Fisioterapeutas;
import modelos.Jugadores;
import modelos.Directiva;

/**
 *
 * @author david
 */
public class Pruebas {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        try {

            Jugadores jugadorPrimero = new Jugadores("Paco", 22, "delantero", 20, 200.00, false, 34);
            System.out.println(jugadorPrimero.toString());
            Jugadores jugadorSegundo = new Jugadores("Pepe", 22, "delantero", 20, 200.00, false, 34);
            System.out.println(jugadorSegundo.toString());          
            Fisioterapeutas fisioPrimero = new Fisioterapeutas("Paxuli", 49, "recuperador", 10.500);
            System.out.println(fisioPrimero.toString());
            Entrenadores entrenadorPrimero = new Entrenadores("Zizou", 30, "Mister", 20.000, 40);
            System.out.println(entrenadorPrimero.toString());
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
