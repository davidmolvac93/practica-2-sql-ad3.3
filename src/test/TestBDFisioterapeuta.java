/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import controlador.FisioterapeutaControlador;
import controller.FisioterapeutaJDBC;
import java.sql.SQLException;
import modelos.Fisioterapeutas;

/**
 *
 * @author david
 */
public class TestBDFisioterapeuta {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws ClassNotFoundException{
        
        int contador = 0;
        int a = 3;
        int b = 10;
        Class.forName("com.mysql.jdbc.Driver");
        FisioterapeutaJDBC fisio = new FisioterapeutaJDBC(FisioterapeutaControlador.conectar());
        
        
//INSERTAR FISIOTERAPEUTA
        /*
        System.out.println("");
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");
        System.out.println("Nombre del test: " + (contador + 1) + " Controller Modelo 03");
        System.out.println("Query que se ejecuta: " + "insert into fisioterapeuta (nombre, edad, especialidad , salario)values"); //iNTRODUCIMOS LOS VALORES      
        System.out.println("Query que se ejecuta: SELECT_LAST_ID"); //
        System.out.println("Salida esperada: " + fisio.insertFisio("Palomo", 39, "Masaje", 3.000));
        System.out.println("Salida obtenida: true");
        System.out.println("Resultado correcto");
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");       
        */
        
        
        //BORRAR FISIO
        /*
        System.out.println("");
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");
        System.out.println("Nombre del test: " + (contador + 1) + " Controller Modelo 04");
        System.out.println("Query que se ejecuta: delete from fisioterapeutas where idFisioterapeuta = 1001"); //Ponemos un limite y desde donde partimos
        System.out.println("Query que se ejecuta:  DELETE_FISIOTERAPEUTAS");  
        System.out.println("Salida esperada: true" );
        System.out.println("Salida obtenida: true");
        if(fisio.deleteFisio(1001)){
            System.out.println("Resultado correcto");
        }else{
            System.out.println("Resultado incorrecto");
        }
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");
        */
       
        
        
        //SELECCIONAR TODOS LOS FISIOS
        /*
        System.out.println("");
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");
        System.out.println("Nombre del test: " + (contador + 1) + " Controller Modelo 05");
        System.out.println("Query que se ejecuta: " + "select * from fisioterapeutas"); //Ponemos un limite y desde donde partimos
        System.out.println("Query que se ejecuta:  SELECT_ALL_FISIOTERAPEUTAS");
        System.out.println("Salida esperada: " + fisio.allFisoterapeutas());
        System.out.println("Salida obtenida: " + fisio.allFisoterapeutas());
        System.out.println("Resultado correcto");
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");
      */
        
        //LISTAR LOS FISIO
        /*
        System.out.println("");
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");
        System.out.println("Nombre del test: " + (contador + 1) + " Controller Modelo 01");
        System.out.println("Query que se ejecuta: " + "select * from fisioterapeutas order by idFisioterapeuta limit 0 offset 10"); //Ponemos un limite y desde donde partimos
        System.out.println("Query que se ejecuta: + SELECT_ALL_ENTRENADORES_PAGINATION");
        System.out.println("Salida esperada: " + 10);
        System.out.println("Salida obtenida: " + b);
        System.out.println(fisio.listaFisios(a, b));
        System.out.println("Resultado correcto");
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");
        */
        
        
        //OBTENER FISIO POR SU NOMBRE
     
        /*
        System.out.println("");
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");
        System.out.println("Nombre del test: " + (contador + 1) + " Controller Modelo 06");
        System.out.println("Query que se ejecuta: select * from fisioterapeutas where nombre LIKE ? order by idFisioterapeuta limit ? offset ?"); //Al fisio que queramos buscar le pasamos en a su ID
        System.out.println("Query que se ejecuta:  SELECT_FISIOTERAPEUTAS_BY_NOMBRE");
        System.out.println("Salida esperada: " + fisio.getFisoterapeutasLike("be", 25, 0).size());
        System.out.println("Salida obtenida: " + fisio.getFisoterapeutasLike("be", 25, 0).size());
        System.out.println("Resultado correcto");
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");         
        */
        
        
        //OBTENER FISIO
        
        /*
        System.out.println("");
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");
        System.out.println("Nombre del test: " + (contador + 1) + " Controller Modelo 02");
        System.out.println("Query que se ejecuta: " + "select * from fisioterapeutas where idFisioterapeuta = 3"); //Al jugador que queramos buscar le pasamos en a su ID
        System.out.println("Query que se ejecuta:  SELECT_FISIO");  
        System.out.println("Salida esperada: " + a);
        System.out.println("Salida obtenida: " + fisio.getFisio(a).getIdFisioterapueta());
        
        if(fisio.getFisio(a).getIdFisioterapueta()==a){
            System.out.println("Resultado correcto");
        }else{
            System.out.println("Resultado incorrecto");
        }
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");
*/
         
        
        
        //ACTUALIZAR FISIO
        /*
        System.out.println("");
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");
        System.out.println("Nombre del test: " + (contador + 1) + " Controller Modelo 08");
        System.out.println("Query que se ejecuta: update fisioterapeutas SET nombre = ?, edad= ?, especialidad = ? , salario = ?  where idFisioterapeuta = ?"); //Al jugador que queramos buscar le pasamos en a su ID
        System.out.println("Query que se ejecuta:  UPDATE_FISIOTERAPEUTAS"); 
        Fisioterapeutas fisio1 = new Fisioterapeutas("Javier Mallo",40,"Preparador Fisico",5200);
        fisio1.setIdFisioterapueta(3);                                     //FUNCIONA
        System.out.println(fisio.updateFisio(fisio1)); 
        System.out.println("Resultado correcto");
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");
        */
        
        //OBTENER FISIO POR EDAD
        /*
        System.out.println("");
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");
        System.out.println("Nombre del test: " + (contador + 1) + " Controller Modelo 07");
        System.out.println("Query que se ejecuta: select * from fisioterapeutas where edad = ? order by idFisioterapeuta limit ? offset ?"); //Al jugador que queramos buscar le pasamos en a su ID
        System.out.println("Query que se ejecuta:  SELECT_FISIOTERAPEUTAS_BY_EDAD");
        System.out.println("Salida esperada: " + fisio.getFisoterapeutasByEdad(39, 25, 0));
        System.out.println("Salida obtenida: " + fisio.getFisoterapeutasByEdad(39, 25, 0).size());
        System.out.println("Resultado correcto");
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");    
        */
        
        
        //OBTENER FISIO POR ULTIMO ID
  
        /*
        System.out.println("");
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");
        System.out.println("Nombre del test: " + (contador + 1) + " Controller Modelo 09");
        System.out.println("Query que se ejecuta: select MAX(idFisioterapeuta) as idFisioterapeuta from fisioterapeuta"); //Al entreandor que queramos buscar le pasamos en a su ID
        System.out.println("Query que se ejecuta:  SELECT_LAST_ID"); 
        System.out.println("Salida esperada " + fisio.lastID());
        System.out.println("Salida obtenida " + fisio.lastID());
        System.out.println("Resultado correcto ");
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");        
        */
    }
    
    
}
