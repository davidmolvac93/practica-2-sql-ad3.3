/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import controlador.entrenadoresControlador;
import controller.EntrenadoresJDBC;
import modelos.Entrenadores;



/**
 *
 * @author david
 */
public class TestBDEntrenador {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws ClassNotFoundException {
                int contador = 0;
        int a = 3;
        int b = 10;
        Class.forName("com.mysql.jdbc.Driver");
        EntrenadoresJDBC entrenador = new EntrenadoresJDBC(entrenadoresControlador.conectar());

        
        //INSERTAR ENTRENADOR
        /*
        System.out.println("");
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");
        System.out.println("Nombre del test: " + (contador + 1) + " Controller Modelo 03");
        System.out.println("Query que se ejecuta: " + "insert into entrenadores (nombre, edad, cargo , salario, titulos)values"); //iNTRODUCIMOS LOS VALORES      
        System.out.println("Query que se ejecuta: SELECT_LAST_ID"); //
        System.out.println("Salida esperada: " + entrenador.insertEntrenadores("Arrigo Sachi", 76, "Asistente de entrenador", 8000, 4));
        System.out.println("Salida obtenida: true");
        System.out.println("Resultado correcto");
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");       
        */
        
        
        //BORRAR ENTRENADOR
        /*
        System.out.println("");
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");
        System.out.println("Nombre del test: " + (contador + 1) + " Controller Modelo 04");
        System.out.println("Query que se ejecuta: delete from entrenadores where idEntrenador = 1001"); //Ponemos un limite y desde donde partimos
        System.out.println("Query que se ejecuta:  DELETE_ENTRENADORES");  
        System.out.println("Salida esperada: true" );
        System.out.println("Salida obtenida: true");
        if(entrenador.deleteEntrenadores(1001)){
            System.out.println("Resultado correcto");
        }else{
            System.out.println("Resultado incorrecto");
        }
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");
*/
        
       
        
        
        //SELECCIONAR TODOS LOS ENTRENADORES
        /*
        System.out.println("");
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");
        System.out.println("Nombre del test: " + (contador + 1) + " Controller Modelo 05");
        System.out.println("Query que se ejecuta: " + "select * from entrenadores"); //Ponemos un limite y desde donde partimos
        System.out.println("Query que se ejecuta:  SELECT_ALL_ENTRENADORES");
        System.out.println("Salida esperada: " + entrenador.allEntrenadores());
        System.out.println("Salida obtenida: " + entrenador.allEntrenadores());
        System.out.println("Resultado correcto");
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");
    */    
        
        //LISTAR LOS ENTRENADOR
        /*
        System.out.println("");
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");
        System.out.println("Nombre del test: " + (contador + 1) + " Controller Modelo 01");
        System.out.println("Query que se ejecuta: " + "select * from entrenadores order by idEntrenador limit 0 offset 10"); //Ponemos un limite y desde donde partimos
        System.out.println("Query que se ejecuta: + SELECT_ALL_ENTRENADORES_PAGINATION");
        System.out.println("Salida esperada: " + 10);
        System.out.println("Salida obtenida: " + b);
        System.out.println(entrenador.listaEntreandores(a, b));
        System.out.println("Resultado correcto");
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");
        */
        
        
        //OBTENER ENTRENADOR POR SU NOMBRE
     
        /*
        System.out.println("");
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");
        System.out.println("Nombre del test: " + (contador + 1) + " Controller Modelo 06");
        System.out.println("Query que se ejecuta: select * from entrenadores where nombre LIKE ? order by idEntrenador limit ? offset ?"); //Al jugador que queramos buscar le pasamos en a su ID
        System.out.println("Query que se ejecuta:  SELECT_ENTRENADORES_BY_NOMBRE");
        System.out.println("Salida esperada: " + entrenador.getEntrenadorLike("ha", 25, 0).size());
        System.out.println("Salida obtenida: " + entrenador.getEntrenadorLike("ha", 25, 0).size());
        System.out.println("Resultado correcto");
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");         
        */
        
        
        //OBTENER ENTRENADOR
        
        /*
        System.out.println("");
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");
        System.out.println("Nombre del test: " + (contador + 1) + " Controller Modelo 02");
        System.out.println("Query que se ejecuta: " + "select * from entrenadores where idEntrenador = 3"); //Al jugador que queramos buscar le pasamos en a su ID
        System.out.println("Query que se ejecuta:  SELECT_ENTRENADORES");  
        System.out.println("Salida esperada: " + a);
        System.out.println("Salida obtenida: " + entrenador.getEntrenadores(a).getIdEntrenador());
        
        if(entrenador.getEntrenadores(a).getIdEntrenador()==a){
            System.out.println("Resultado correcto");
        }else{
            System.out.println("Resultado incorrecto");
        }
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");

         */
        
        
        //ACTUALIZAR ENTRENADORES
        /*
        System.out.println("");
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");
        System.out.println("Nombre del test: " + (contador + 1) + " Controller Modelo 08");
        System.out.println("Query que se ejecuta: update entrenadores SET nombre = ?, edad= ?, cargo = ? , salario = ? , titulos = ?  where idEntrenador = ?"); //Al jugador que queramos buscar le pasamos en a su ID
        System.out.println("Query que se ejecuta:  UPDATE_ENTRENADORES"); 
        Entrenadores entrena1 = new Entrenadores("Hamidou Msiadie",55,"Ayudante tecnico",5200,9);
        entrena1.setIdEntrenador(1);                                     //FUNCIONA
        System.out.println(entrenador.updateEntrenadores(entrena1)); 
        System.out.println("Resultado correcto");
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");
        */
        
        //OBTENER ENTRENADOR POR TITULOS
        /*
        System.out.println("");
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");
        System.out.println("Nombre del test: " + (contador + 1) + " Controller Modelo 07");
        System.out.println("Query que se ejecuta: select * from entrenadores where titulos = ? order by idEntrenador limit ? offset ?"); //Al jugador que queramos buscar le pasamos en a su ID
        System.out.println("Query que se ejecuta:  SELECT_ENTRENADORES_BY_GOLES");
        System.out.println("Salida esperada: " + entrenador.getEntrenadorByTitulos(9, 25, 0));
        System.out.println("Salida obtenida: " + entrenador.getEntrenadorByTitulos(9, 25, 0).size());
        System.out.println("Resultado correcto");
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");    
        */
        
        
        //OBTENER ENTRENADOR POR ULTIMO ID
  
        /*
        System.out.println("");
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");
        System.out.println("Nombre del test: " + (contador + 1) + " Controller Modelo 09");
        System.out.println("Query que se ejecuta: select MAX(idEntrenador) as idEntrenador from entrenadores"); //Al entreandor que queramos buscar le pasamos en a su ID
        System.out.println("Query que se ejecuta:  SELECT_LAST_ID"); 
        System.out.println("Salida esperada " + entrenador.lastID());
        System.out.println("Salida obtenida " + entrenador.lastID());
        System.out.println("Resultado correcto ");
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*");        
        */
    }
    
}
