/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

/**
 *
 * @author david
 */
public class Entrena {
    
    private int idEntrenador_entrenadores;
    private int idJugador_jugadores;
    
    
    /*
    
    */

    public Entrena(int idEntrenador_entrenadores, int idJugador_jugadores) {
        this.idEntrenador_entrenadores = idEntrenador_entrenadores;
        this.idJugador_jugadores = idJugador_jugadores;
    }

    public int getIdEntrenador_entrenadores() {
        return idEntrenador_entrenadores;
    }

    public void setIdEntrenador_entrenadores(int idEntrenador_entrenadores) {
        this.idEntrenador_entrenadores = idEntrenador_entrenadores;
    }

    public int getIdJugador_jugadores() {
        return idJugador_jugadores;
    }

    public void setIdJugador_jugadores(int idJugador_jugadores) {
        this.idJugador_jugadores = idJugador_jugadores;
    }

    @Override
    public String toString() {
        return "Entrena{" + "idEntrenador_entrenadores=" + idEntrenador_entrenadores + ", idJugador_jugadores=" + idJugador_jugadores + '}';
    }
    
    
}
