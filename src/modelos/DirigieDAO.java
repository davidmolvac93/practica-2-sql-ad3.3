/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

import java.util.List;

/**
 *
 * @author david
 */
public interface DirigieDAO {

    public List<Dirige> listaDirige(int desde, int cuantos);

    public boolean insertDirige(String nombre);

    public boolean updateDirige(Dirige dirigidos);

    public boolean deleteDirige(int idDirectiva_directiva, int idEntrenador_entrenadores);

    public Entrena getEntrenos(int idDirectiva_directiva, int idEntrenador_entrenadores);

    public List<Dirige> getDirigeLike(String condicion); 
}
