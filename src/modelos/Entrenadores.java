/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

/**
 *
 * @author david
 */
public class Entrenadores {
    
    public int idEntrenador;
    public String nombre;
    public int edad;
    public String cargo;
    public double salario;
    public int titulos;

    public Entrenadores(String nombre, int edad, String cargo, double salario, int titulos) {
        this.nombre = nombre;
        this.edad = edad;
        this.cargo = cargo;
        this.salario = salario;
        this.titulos = titulos;
    }

        public int getIdEntrenador() {
        return idEntrenador;
    }

    public void setIdEntrenador(int idEntrenador) {
        this.idEntrenador = idEntrenador;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }

    public int getTitulos() {
        return titulos;
    }

    public void setTitulos(int titulos) {
        this.titulos = titulos;
    }

    @Override
    public String toString() {
        return "Entrenadores{" + "idEntrenador=" + idEntrenador + ", nombre=" + nombre + ", edad=" + edad + ", cargo=" + cargo + ", salario=" + salario + ", titulos=" + titulos + '}';
    }
    
    
    
    
}
