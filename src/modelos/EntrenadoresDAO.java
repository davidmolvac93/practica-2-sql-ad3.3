/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

import java.util.List;

/**
 *
 * @author david
 */
public interface EntrenadoresDAO {

    public List<Entrenadores> listaEntreandores(int desde, int cuantos);

    public boolean insertEntrenadores(String nombre, int edad, String cargo, int salario, int titulos);

    public boolean updateEntrenadores(Entrenadores entrenador);

    public boolean deleteEntrenadores(int idEntrenador);

    public Entrenadores getEntrenadores(int idEntrenador);

    public List<Entrenadores> getEntrenadorLike(String nombre, int desde, int cuantos);

    public List<Entrenadores> getEntrenadorByTitulos(int titulos, int desde, int cuantos);

    public int allEntrenadores();

    public int lastID();

}
