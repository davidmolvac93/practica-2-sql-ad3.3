/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

import java.util.List;

/**
 *
 * @author david
 */
public interface FisioterapeutasDAO {

    public List<Fisioterapeutas> listaFisios(int desde, int cuantos);

    public boolean insertFisio(String nombre, int edad, String especialidad, double salario); //poner los valores del constructor

    public boolean updateFisio(Fisioterapeutas fisio);

    public boolean deleteFisio(int idFisioterapeuta);

    public Fisioterapeutas getFisio(int idFisioterapeuta);
    
    public List<Fisioterapeutas> getFisoterapeutasLike(String nombre, int desde, int cuantos);

    public List<Fisioterapeutas> getFisoterapeutasByEdad(int edad, int desde, int cuantos);

    public int allFisoterapeutas();

    public int lastID();
}
