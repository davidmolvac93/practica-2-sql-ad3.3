/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

/**
 *
 * @author david
 */
public class Recupera {
    
    private int idJugador_jugadores;
    private int idFisioterapeuta_fisioterapeutas;

    public Recupera(int idJugador_jugadores, int idFisioterapeuta_fisioterapeutas) {
        this.idJugador_jugadores = idJugador_jugadores;
        this.idFisioterapeuta_fisioterapeutas = idFisioterapeuta_fisioterapeutas;
    }

    public int getIdJugador_jugadores() {
        return idJugador_jugadores;
    }

    public void setIdJugador_jugadores(int idJugador_jugadores) {
        this.idJugador_jugadores = idJugador_jugadores;
    }

    public int getIdFisioterapeuta_fisioterapeutas() {
        return idFisioterapeuta_fisioterapeutas;
    }

    public void setIdFisioterapeuta_fisioterapeutas(int idFisioterapeuta_fisioterapeutas) {
        this.idFisioterapeuta_fisioterapeutas = idFisioterapeuta_fisioterapeutas;
    }

    @Override
    public String toString() {
        return "Recupera{" + "idJugador_jugadores=" + idJugador_jugadores + ", idFisioterapeuta_fisioterapeutas=" + idFisioterapeuta_fisioterapeutas + '}';
    }
    
    
}
