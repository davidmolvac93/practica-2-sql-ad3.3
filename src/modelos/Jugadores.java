/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

/**
 *
 * @author david
 */
public class Jugadores {
    
    private int idJugador;
    private String nombre;
    private int edad;
    private String posicion;
    private int goles;
    private double salarioSemanal;
    private boolean lesionado;
    private int dorsal;

    public Jugadores(String nombre, int edad, String posicion, int goles, double salarioSemanal, boolean lesionado, int dorsal) {
        this.nombre = nombre;
        this.edad = edad;
        this.posicion = posicion;
        this.goles = goles;
        this.salarioSemanal = salarioSemanal;
        this.lesionado = lesionado;
        this.dorsal = dorsal;
    }

    public int getIdJugador() {
        return idJugador;
    }

    public void setIdJugador(int idJugador) {
        this.idJugador = idJugador;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getPosicion() {
        return posicion;
    }

    public void setPosicion(String posicion) {
        this.posicion = posicion;
    }

    public int getGoles() {
        return goles;
    }

    public void setGoles(int goles) {
        this.goles = goles;
    }

    public double getSalarioSemanal() {
        return salarioSemanal;
    }

    public void setSalarioSemanal(double salarioSemanal) {
        this.salarioSemanal = salarioSemanal;
    }

    public boolean isLesionado() {
        return lesionado;
    }

    public void setLesionado(boolean lesionado) {
        this.lesionado = lesionado;
    }

    public int getDorsal() {
        return dorsal;
    }

    public void setDorsal(int dorsal) {
        this.dorsal = dorsal;
    }

    @Override
    public String toString() {
        return "Jugadores{" + "idJugador=" + idJugador + ", nombre=" + nombre + ", edad=" + edad + ", posicion=" + posicion + ", goles=" + goles + ", salarioSemanal=" + salarioSemanal + ", lesionado=" + lesionado + ", dorsal=" + dorsal + '}';
    }
    
    
}
