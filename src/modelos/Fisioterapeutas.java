/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

/**
 *
 * @author david
 */
public class Fisioterapeutas {
    
    private int idFisioterapueta;
    private String nombre;
    private int edad;
    private String especialidad;
    private double salario;

    public Fisioterapeutas(String nombre, int edad, String especialidad, double salario) {
        this.nombre = nombre;
        this.edad = edad;
        this.especialidad = especialidad;
        this.salario = salario;
    }

    public int getIdFisioterapueta() {
        return idFisioterapueta;
    }

    public void setIdFisioterapueta(int idFisioterapueta) {
        this.idFisioterapueta = idFisioterapueta;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEspecialidad() {
        return especialidad;
    }

    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    
    @Override
    public String toString() {
        return "Fisoterapeutas{" + "idFisioterapueta=" + idFisioterapueta + ", nombre=" + nombre + ", edad=" + edad + ", especialidad=" + especialidad + ", salario=" + salario + '}';
    }
    
    
}
