/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.sql.Connection;
import java.sql.SQLException;
import modelo.Conexion;

/**
 *
 * @author david
 */
public class DirectivaControlador {

    public static Connection conectar() {
        Conexion myConexionDriverManager = null;
        Connection myConnection = null;

        try {
            System.out.println("Leyendo el fichero de conexión.");
            myConexionDriverManager = new Conexion();
        } catch (Exception e) {
            System.err.println("Problem reading properties file ");
            e.printStackTrace();
        }

        try {
            myConnection = myConexionDriverManager.getConexion();
        } catch (SQLException e) {
            e.printStackTrace(System.err);
        } catch (Exception e) {
            e.printStackTrace(System.err);
        }

        return myConnection;
    }

    public static void desconectar(Connection con) {
        Conexion.closeConnection(con);
    }
}
