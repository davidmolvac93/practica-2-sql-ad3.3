/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vistas;

import controlador.DirectivaControlador;
import controlador.FisioterapeutaControlador;
import controlador.entrenadoresControlador;
import controlador.jugadorControlador;
import controller.DirectivaJDBC;
import controller.EntrenadoresJDBC;
import controller.FisioterapeutaJDBC;
import controller.jugadoresJDBC;
import java.util.Scanner;
import modelos.Directiva;
import modelos.Entrenadores;
import modelos.Fisioterapeutas;
import modelos.Jugadores;

/**
 *
 * @author david
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws ClassNotFoundException {

        Class.forName("com.mysql.jdbc.Driver");

        DirectivaJDBC directivo = new DirectivaJDBC(DirectivaControlador.conectar());
        Directiva directivo1 = new Directiva("Juan Pablo", 66, 4033, "Bobo");
        directivo1.setIdDirectiva(3);

        EntrenadoresJDBC entrenador = new EntrenadoresJDBC(entrenadoresControlador.conectar());
        Entrenadores entrenador1 = new Entrenadores("Hamidou Msiadie", 55, "Ayudante tecnico", 5200, 9);
        entrenador1.setIdEntrenador(1);

        jugadoresJDBC jugador = new jugadoresJDBC(jugadorControlador.conectar());
        Jugadores jugador1 = new Jugadores("Thibaut Courtois", 30, "Portero", 0, 10, false, 100);
        jugador1.setIdJugador(1);

        FisioterapeutaJDBC fisio = new FisioterapeutaJDBC(FisioterapeutaControlador.conectar());
        Fisioterapeutas fisio1 = new Fisioterapeutas("Roberto Vazquez", 40, "Preparador de porteros", 5000);
        fisio1.setIdFisioterapueta(2);

        boolean cerramos = false;

        System.out.println("Bienvenido a nuestra base de datos, aqui comenzaremos con las consultas pero primero \n"
                + "Necesitamos saber sobre que tabla quieres interactuar por favor\n"
                + "1º Tabla Jugadores\n"
                + "2º Tabla Entrenadores\n"
                + "3º Tabla Fisioterapeutas\n"
                + "4º Tabla Directiva");

        String tp = " ";
        Scanner sc = new Scanner(System.in);

        while (!cerramos) {

            System.out.println("Para seleccionar la tabla sobre la que queremos hacer las consultas tan solo introduce el nombre de la tabla a continuacion");

            tp = " ";
            tp = sc.nextLine();

            System.out.println("");

            if (tp.equalsIgnoreCase("Jugadores")) {

                System.out.println("Bienvenido a jugadores\n"
                        + "A continuacion te damos el listado de acciones para desarrollar en esta tabla\n"
                        + "Tan solo debes poner el valor de la derecha de cada proceso para hacer la consulta\n"
                        + "1º Obtener ultimo id   ----> last\n"
                        + "2º Obtener a un jugador buscandolo por su ID   ---->  get\n"
                        + "3º Insertar un nuevo jugador  ------>   insert\n"
                        + "4º Borrar un jugador   ------>   delete\n"
                        + "5º Ver todos los jugadores    ---->  all\n"
                        + "6º Listado con los jugadores   ---->  list\n"
                        + "7º Actualizar al jugador   ----> update\n"
                        + "8º Obtener jugador por nombre  ---->   name\n"
                        + "9º Obtener jugadir por goles   ------>  goal\n"
                        + "10º Finalizar las consultas  -------> finish");

                String jug = " ";
                String jugNombre = " ";
                String jugPosicion = " ";
                int juga;
                int jugaEdad;
                int jugaGoles;
                int jugaSalario;
                int jugaDorsal;
                boolean lesionado;
                Scanner scjug = new Scanner(System.in);
                Scanner scjuga = new Scanner(System.in);
                Scanner sclesion = new Scanner(System.in);
                jug = scjug.nextLine();

                System.out.println("");
                switch (jug) {

                    case "last":
                        System.out.println("*******************");
                        jugador.lastID();                                   //DONE
                        System.out.println("*******************");
                        break;

                    case "get":
                        System.out.println("Introduce el ID que deseas buscar");
                        juga = scjuga.nextInt();
                        System.out.println("*******************");
                        jugador.getJugador(juga);                          //DONE
                        System.out.println("*******************");
                        break;

                    case "insert":
                        System.out.println("Vamos a introducir al nuevo jugador, rellena los siguientes datos\n"
                                + "Nombre: ");
                        jugNombre = scjug.nextLine();
                        System.out.println("Edad: ");
                        jugaEdad = scjuga.nextInt();
                        System.out.println("Posicion: ");
                        jugPosicion = scjug.nextLine();
                        System.out.println("Goles: ");
                        jugaGoles = scjuga.nextInt();
                        System.out.println("Salario: ");
                        jugaSalario = scjuga.nextInt();                     //DONE
                        System.out.println("Lesionado: ");
                        lesionado = sclesion.nextBoolean();
                        System.out.println("Dorsal: ");
                        jugaDorsal = scjuga.nextInt();
                        System.out.println("*******************");
                        jugador.insertJugador(jugNombre, jugaEdad, jugPosicion, jugaGoles, jugaSalario, cerramos, jugaDorsal);
                        System.out.println("*******************");
                        break;

                    case "delete":
                        System.out.println("Introduce el ID del jugador que desear eliminar");
                        juga = scjuga.nextInt();
                        System.out.println("*******************");
                        jugador.deleteJugador(juga);                        //DONE
                        System.out.println("*******************");
                        break;

                    case "all":
                        System.out.println("*******************");
                        System.out.println("Numero de jugadores totales");
                        jugador.allJugadores();                             //DONE
                        System.out.println("*******************");
                        break;

                    case "list":
                        System.out.println("Introduce desde que ID queremos hacer la consulta");
                        juga = scjuga.nextInt();
                        System.out.println("Introduce hasta que ID queremos hacer la consulta");
                        jugaEdad = scjuga.nextInt();
                        System.out.println("*******************");                  //DONE
                        jugador.listaJugadores(juga, jugaEdad);
                        System.out.println("*******************");
                        break;

                    case "update":
                        System.out.println("Vamos a actualizar a un jugador, necesitamos su\n"
                                + "Nombre: ");
                        jugNombre = scjug.nextLine();
                        System.out.println("Edad: ");
                        jugaEdad = scjuga.nextInt();
                        System.out.println("Posicion: ");
                        jugPosicion = scjug.nextLine();
                        System.out.println("Goles: ");
                        jugaGoles = scjuga.nextInt();
                        System.out.println("Salario: ");
                        jugaSalario = scjuga.nextInt();                     //DONE
                        System.out.println("Lesionado: ");
                        lesionado = sclesion.nextBoolean();
                        System.out.println("Dorsal: ");
                        jugaDorsal = scjuga.nextInt();
                        System.out.println("*******************");
                        jugador.updateJugador(jugador1);
                        System.out.println("*******************");
                        break;

                    case "name":
                        System.out.println("Introduce el nombre que queremos buscar");
                        jugNombre = scjug.nextLine();
                        System.out.println("Introduce desde que ID queremos hacer la consulta");
                        juga = scjuga.nextInt();
                        System.out.println("Introduce hasta que ID queremos hacer la consulta");
                        jugaEdad = scjuga.nextInt();                            //DONE  
                        System.out.println("*******************");
                        jugador.getJugadorLike(jug, juga, jugaEdad);
                        System.out.println("*******************");
                        break;

                    case "goal":
                        System.out.println("Introduce el numero de goles que quieres buscar");
                        jugaGoles = scjuga.nextInt();
                        System.out.println("Introduce desde que ID queremos hacer la consulta");
                        juga = scjuga.nextInt();
                        System.out.println("Introduce hasta que ID queremos hacer la consulta");
                        jugaEdad = scjuga.nextInt();                                                //DONE
                        System.out.println("*******************");
                        jugador.getJugadorByGoles(jugaGoles, juga, jugaEdad);
                        System.out.println("*******************");
                        break;

                    case "finish":
                        System.out.println("Finalizamos la consulta de esta tabla");
                        break;
                }

            }
            if (tp.equalsIgnoreCase("Entrenadores")) {

                System.out.println("Bienvenido a entrenadores\n"
                        + "A continuacion te damos el listado de acciones para desarrollar en esta tabla\n"
                        + "Tan solo debes poner el valor de la derecha de cada proceso para hacer la consulta\n"
                        + "1º Obtener ultimo id   ----> last\n"
                        + "2º Obtener a un entrenador buscandolo por su ID   ---->  get\n"
                        + "3º Insertar un nuevo entrenador  ------>   insert\n"
                        + "4º Borrar un entrenador   ------>   delete\n"
                        + "5º Ver todos los entrenador    ---->  all\n"
                        + "6º Listado con los entrenador   ---->  list\n"
                        + "7º Actualizar al entrenador   ----> update\n"
                        + "8º Obtener entrenador por nombre  ---->   name\n"
                        + "9º Obtener entrenador por titulos   ------>  title\n"
                        + "10º Finalizar las consultas  -------> finish");

                String ent = " ";
                String entNombre = " ";
                String entCargo = " ";
                int entre;
                int entreEdad;
                int entreSalario;
                int entreTitulos;

                Scanner scent = new Scanner(System.in);
                Scanner scentre = new Scanner(System.in);

                ent = scent.nextLine();

                System.out.println("");

                switch (ent) {

                    case "last":
                        System.out.println("*******************");
                        entrenador.lastID();
                        System.out.println("*******************");
                        break;

                    case "get":
                        System.out.println("Introduce el ID para buscar el entrenador por favor");
                        entre = scentre.nextInt();
                        System.out.println("*******************");
                        entrenador.getEntrenadores(entre);
                        System.out.println("*******************");
                        break;

                    case "insert":
                        System.out.println("Vamos a introducir al nuevo entrenador, rellena los siguientes datos\n"
                                + "Nombre: ");
                        entNombre = scent.nextLine();
                        System.out.println("Edad: ");
                        entreEdad = scentre.nextInt();
                        System.out.println("Cargo: ");
                        entCargo = scent.nextLine();
                        System.out.println("Salario: ");
                        entreSalario = scentre.nextInt();
                        System.out.println("Titulos: ");
                        entreTitulos = scentre.nextInt();
                        System.out.println("*******************");
                        entrenador.insertEntrenadores(entNombre, entreEdad, entCargo, entreSalario, entreTitulos);
                        System.out.println("*******************");
                        break;

                    case "delete":
                        System.out.println("Introduce el ID del entrenador que deseas eliminar");
                        entre = scentre.nextInt();
                        System.out.println("*******************");
                        entrenador.deleteEntrenadores(entre);
                        System.out.println("*******************");
                        break;

                    case "all":
                        System.out.println("*******************");
                        System.out.println("Numero de entrenadores totales");
                        entrenador.allEntrenadores();
                        System.out.println("*******************");
                        break;

                    case "list":
                        System.out.println("Introduce desde que ID queremos hacer la consukta");
                        entre = scentre.nextInt();
                        System.out.println("Introduce hasta que ID queremos hacer la consulta");
                        entreEdad = scentre.nextInt();
                        System.out.println("*******************");
                        entrenador.listaEntreandores(entre, entreEdad);
                        System.out.println("*******************");
                        break;

                    case "update":
                        System.out.println("Vamos a actualizar a un entrenador, necesitamos su\n"
                                + "Nombre: ");
                        entNombre = scent.nextLine();
                        System.out.println("Edad: ");
                        entreEdad = scentre.nextInt();
                        System.out.println("Cargo: ");
                        entCargo = scent.nextLine();
                        System.out.println("Salario: ");
                        entreSalario = scentre.nextInt();
                        System.out.println("Titulos: ");
                        entreTitulos = scentre.nextInt();
                        System.out.println("*******************");
                        entrenador.updateEntrenadores(entrenador1);
                        System.out.println("*******************");
                        break;

                    case "name":
                        System.out.println("Introduce el nombre que queremos buscar");
                        entNombre = scent.nextLine();
                        System.out.println("Introduce desde que ID queremos hacer la consulta");
                        entre = scentre.nextInt();
                        System.out.println("Introduce hasta que ID queremos hacer la consulta");
                        entreEdad = scentre.nextInt();
                        System.out.println("*******************");
                        entrenador.getEntrenadorLike(entNombre, entre, entreEdad);
                        System.out.println("*******************");
                        break;

                    case "title":
                        System.out.println("Introduce el numero de titulos que quieres buscar");
                        entreTitulos = scentre.nextInt();
                        System.out.println("Introduce desde que ID queremos hacer la consulta");
                        entre = scentre.nextInt();
                        System.out.println("Introduce hasta que ID queremos hacer la consulta");
                        entreEdad = scentre.nextInt();
                        System.out.println("*******************");
                        entrenador.getEntrenadorByTitulos(entreTitulos, entre, entreEdad);
                        System.out.println("*******************");
                        break;

                    case "finish":
                        System.out.println("Finalizamos");
                        break;

                }
            }
            if (tp.equalsIgnoreCase("Fisioterapeutas")) {

                System.out.println("Bienvenido a fisioterapeutas\n"
                        + "A continuacion te damos el listado de acciones para desarrollar en esta tabla\n"
                        + "Tan solo debes poner el valor de la derecha de cada proceso para hacer la consulta\n"
                        + "1º Obtener ultimo id   ----> last\n"
                        + "2º Obtener a un fisioterapeuta buscandolo por su ID   ---->  get\n"
                        + "3º Insertar un nuevo fisioterapeuta  ------>   insert\n"
                        + "4º Borrar un fisioterapeuta   ------>   delete\n"
                        + "5º Ver todos los fisioterapeuta    ---->  all\n"
                        + "6º Listado con los fisioterapeuta   ---->  list\n"
                        + "7º Actualizar al fisioterapeuta   ----> update\n"
                        + "8º Obtener fisioterapeuta por nombre  ---->   name\n"
                        + "9º Obtener fisioterapeuta por edad   ------>  edad\n"
                        + "10º Finalizar las consultas  -------> finish");

                String fis = " ";
                String fisNombre = " ";
                String fisEspecialidad = " ";
                int fisi;
                int fisiEdad;
                int fisiSalario;

                Scanner scfis = new Scanner(System.in);
                Scanner scfisi = new Scanner(System.in);

                fis = scfis.nextLine();

                System.out.println("");

                switch (fis) {

                    case "last":
                        System.out.println("*******************");
                        entrenador.lastID();
                        System.out.println("*******************");
                        break;

                    case "get":
                        System.out.println("Introduce el ID para buscar el fisioterapeuta por favor");
                        fisi = scfisi.nextInt();
                        System.out.println("*******************");
                        fisio.getFisio(fisi);
                        System.out.println("*******************");
                        break;

                    case "insert":
                        System.out.println("Vamos a introducir al nuevo fisioterapeuta, rellena los siguientes datos\n"
                                + "Nombre: ");
                        fisNombre = scfis.nextLine();
                        System.out.println("Edad: ");
                        fisiEdad = scfisi.nextInt();
                        System.out.println("Especialidad: ");
                        fisEspecialidad = sc.nextLine();
                        System.out.println("Salario: ");
                        fisiSalario = scfisi.nextInt();
                        System.out.println("*******************");
                        fisio.insertFisio(fisNombre, fisiEdad, fisEspecialidad, fisiSalario);
                        System.out.println("*******************");
                        break;

                    case "delete":
                        System.out.println("Introduce el ID del fisioterapeuta que deseas eliminar");
                        fisi = scfisi.nextInt();
                        System.out.println("*******************");
                        fisio.deleteFisio(fisi);
                        System.out.println("*******************");
                        break;

                    case "all":
                        System.out.println("*******************");
                        System.out.println("Numero de fisioterapeutas totales");
                        fisio.allFisoterapeutas();
                        System.out.println("*******************");
                        break;

                    case "list":
                        System.out.println("Introduce desde que ID queremos hacer la consukta");
                        fisi = scfisi.nextInt();
                        System.out.println("Introduce hasta que ID queremos hacer la consulta");
                        fisiEdad = scfisi.nextInt();
                        System.out.println("*******************");
                        entrenador.listaEntreandores(fisi, fisiEdad);
                        System.out.println("*******************");
                        break;

                    case "update":
                        System.out.println("Vamos a actualizar a un fisioterapeuta, necesitamos su\n"
                                + "Nombre: ");
                        fisNombre = scfis.nextLine();
                        System.out.println("Edad: ");
                        fisiEdad = scfisi.nextInt();
                        System.out.println("Especialidad: ");
                        fisEspecialidad = scfis.nextLine();
                        System.out.println("Salario: ");
                        fisiSalario = scfisi.nextInt();
                        System.out.println("*******************");
                        fisio.updateFisio(fisio1);
                        System.out.println("*******************");
                        break;

                    case "name":
                        System.out.println("Introduce el nombre que queremos buscar");
                        fisNombre = scfis.nextLine();
                        System.out.println("Introduce desde que ID queremos hacer la consulta");
                        fisi = scfisi.nextInt();
                        System.out.println("Introduce hasta que ID queremos hacer la consulta");
                        fisiEdad = scfisi.nextInt();
                        System.out.println("*******************");
                        fisio.getFisoterapeutasLike(fisNombre, fisi, fisiEdad);
                        System.out.println("*******************");
                        break;

                    case "edad":
                        System.out.println("Introduce la edad  que quieres buscar");
                        fisiEdad = scfisi.nextInt();
                        System.out.println("Introduce desde que ID queremos hacer la consulta");
                        fisi = scfisi.nextInt();
                        System.out.println("Introduce hasta que ID queremos hacer la consulta");
                        fisiSalario = scfisi.nextInt();
                        System.out.println("*******************");
                        fisio.getFisoterapeutasByEdad(fisiEdad, fisi, fisiSalario);
                        System.out.println("*******************");
                        break;

                    case "finish":
                        System.out.println("Finalizamos");
                        break;

                }
            }
            if (tp.equalsIgnoreCase("Directiva")) {

                System.out.println("Bienvenido a directiva\n"
                        + "A continuacion te damos el listado de acciones para desarrollar en esta tabla\n"
                        + "Tan solo debes poner el valor de la derecha de cada proceso para hacer la consulta\n"
                        + "1º Obtener ultimo id   ----> last\n"
                        + "2º Obtener a un directivo buscandolo por su ID   ---->  get\n"
                        + "3º Insertar un nuevo directivo  ------>   insert\n"
                        + "4º Borrar un directivo   ------>   delete\n"
                        + "5º Ver todos los directivo    ---->  all\n"
                        + "6º Listado con los directivo   ---->  list\n"
                        + "7º Actualizar al directivo   ----> update\n"
                        + "8º Obtener directivo por nombre  ---->   name\n"
                        + "9º Obtener directivo por edad   ------>  edad\n"
                        + "10º Finalizar las consultas  -------> finish");

                String dir = " ";
                String dirNombre = " ";
                String dirPuesto = " ";
                int direc;
                int direcEdad;
                int direcSalario;

                Scanner scdir = new Scanner(System.in);
                Scanner scdirec = new Scanner(System.in);

                dir = scdir.nextLine();

                System.out.println("");

                switch (dir) {

                    case "last":
                        System.out.println("*******************");
                        directivo.lastID();
                        System.out.println("*******************");
                        break;

                    case "get":
                        System.out.println("Introduce el ID para buscar el directivo por favor");
                        direc = scdirec.nextInt();
                        System.out.println("*******************");
                        directivo.getDirectivos(direc);
                        System.out.println("*******************");
                        break;

                    case "insert":
                        System.out.println("Vamos a introducir al nuevo directivo, rellena los siguientes datos\n"
                                + "Nombre: ");
                        dirNombre = scdir.nextLine();
                        System.out.println("Edad: ");
                        direcEdad = scdirec.nextInt();
                        System.out.println("Salario: ");
                        direcSalario = scdirec.nextInt();
                        System.out.println("Puesto: ");
                        dirPuesto = sc.nextLine();
                        System.out.println("*******************");
                        directivo.insertDirectivos(dirNombre, direcEdad, direcSalario, dirPuesto);
                        System.out.println("*******************");
                        break;

                    case "delete":
                        System.out.println("Introduce el ID del directivo que deseas eliminar");
                        direc = scdirec.nextInt();
                        System.out.println("*******************");
                        directivo.deleteDirectivos(direc);
                        System.out.println("*******************");
                        break;

                    case "all":
                        System.out.println("*******************");
                        System.out.println("Numero de directivo totales");
                        directivo.allDirectivos();
                        System.out.println("*******************");
                        break;

                    case "list":
                        System.out.println("Introduce desde que ID queremos hacer la consukta");
                        direc = scdirec.nextInt();
                        System.out.println("Introduce hasta que ID queremos hacer la consulta");
                        direcEdad = scdirec.nextInt();
                        System.out.println("*******************");
                        directivo.listaDirectivos(direc, direcEdad);
                        System.out.println("*******************");
                        break;

                    case "update":
                        System.out.println("Vamos a actualizar a un directivo, necesitamos su\n"
                                + "Nombre: ");
                        dirNombre = scdir.nextLine();
                        System.out.println("Edad: ");
                        direcEdad = scdirec.nextInt();
                        System.out.println("Salario: ");
                        direcSalario = scdirec.nextInt();
                        System.out.println("Especialidad: ");
                        dirPuesto = scdir.nextLine();
                        System.out.println("*******************");
                        directivo.updateDirectivos(directivo1);
                        System.out.println("*******************");
                        break;

                    case "name":
                        System.out.println("Introduce el nombre que queremos buscar");
                        dirNombre = scdir.nextLine();
                        System.out.println("Introduce desde que ID queremos hacer la consulta");
                        direc = scdirec.nextInt();
                        System.out.println("Introduce hasta que ID queremos hacer la consulta");
                        direcEdad = scdirec.nextInt();
                        System.out.println("*******************");
                        directivo.getDirectivoLike(dirNombre, direc, direcEdad);
                        System.out.println("*******************");
                        break;

                    case "edad":
                        System.out.println("Introduce la edad  que quieres buscar");
                        direcEdad = scdirec.nextInt();
                        System.out.println("Introduce desde que ID queremos hacer la consulta");
                        direc = scdirec.nextInt();
                        System.out.println("Introduce hasta que ID queremos hacer la consulta");
                        direcSalario = scdirec.nextInt();
                        System.out.println("*******************");
                        fisio.getFisoterapeutasByEdad(direcEdad, direc, direcSalario);
                        System.out.println("*******************");
                        break;

                    case "finish":
                        System.out.println("Finalizamos");
                        break;
                }

            }
        }
    }
}
